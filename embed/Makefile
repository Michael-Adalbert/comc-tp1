# ***************************************************************
# *     Makefile for Atmel AT91SAM7S256 - flash execution       *
# *                                                             *
# *                                                             *
# *   James P Lynch  May 12, 2007                               *
# ***************************************************************

NAME = demo_at91sam7_blink_flash

# variables 
ARCH	= arm-linux-gnueabi-
CC      = $(ARCH)gcc
#LD      = $(ARCH)ld -v
LD		= $(ARCH)gcc
AR      = $(ARCH)ar
AS      = $(ARCH)as
CP      = $(ARCH)objcopy
OD		= $(ARCH)objdump

ARM_EABI_HOME=/nfs/arm
CFLAGS  = -I./ -c -fno-common -O0 -g
AFLAGS  = -ahls -mapcs-32 -o crt.o
LFLAGS  =  -static
# -Map main.map
# -L $(ARM_EABI_HOME)/arm-eabi/lib -L $(ARM_EABI_HOME)/lib/gcc/arm-eabi/4.7.2/ -lg -lgcc -lc
CPFLAGS = --output-target=binary
ODFLAGS	= -x --syms

OBJECTS = main.o timerisr.o timersetup.o isrsupport.o lowlevelinit.o blinker.o


# make target called by Eclipse (Project -> Clean ...)
clean:
	-rm $(OBJECTS) main.out main.bin main.dmp

         
#make target called by Eclipse  (Project -> Build Project)
all:  main.out
	@ echo "...copying"
	$(CP) $(CPFLAGS) main.out main.bin
	$(OD) $(ODFLAGS) main.out > main.dmp

main.out: $(OBJECTS) link.ld
	@ echo "..linking"
	$(LD) -o main.out $(OBJECTS) $(LFLAGS) 

#libc.a libm.a libgcc.a 

main.o: main.c
	@ echo ".compiling"
	$(CC) $(CFLAGS) main.c
	
timerisr.o: timerisr.c
	@ echo ".compiling"
	$(CC) $(CFLAGS) timerisr.c
	
lowlevelinit.o: lowlevelinit.c
	@ echo ".compiling"
	$(CC) $(CFLAGS) lowlevelinit.c
	
timersetup.o: timersetup.c
	@ echo ".compiling"
	$(CC) $(CFLAGS) timersetup.c
	
isrsupport.o: isrsupport.c
	@ echo ".compiling"
	$(CC) $(CFLAGS) isrsupport.c

blinker.o: blinker.c
	@ echo ".compiling"
	$(CC) $(CFLAGS) blinker.c
	

# **********************************************************************************************
#                            FLASH PROGRAMMING                                         
#
# Alternate make target for flash programming only
#
# You must create a special Eclipse make target (program) to run this part of the makefile 
# (Project -> Create Make Target...  then set the Target Name and Make Target to "program")
#
# OpenOCD is run in "batch" mode with a special configuration file and a script file containing
# the flash commands. When flash programming completes, OpenOCD terminates.
#
# Note that the script file of flash commands (script.ocd) is part of the project
#
# Programmers: Martin Thomas, Joseph M Dupre, James P Lynch
# **********************************************************************************************

# specify output filename here (must be *.bin file)
TARGET = main.bin

# specify the directory where openocd executable and configuration files reside
OPENOCD_DIR = 'c:/Program Files/openocd-2007re141/bin/'

# specify OpenOCD executable (pp is for the wiggler, ftd2xx is for the USB debuggers)
#OPENOCD = $(OPENOCD_DIR)openocd-pp.exe
OPENOCD = $(OPENOCD_DIR)openocd-ftd2xx.exe

# specify OpenOCD configuration file (pick the one for your device)
#OPENOCD_CFG = $(OPENOCD_DIR)at91sam7s256-wiggler-flash-program.cfg
#OPENOCD_CFG = $(OPENOCD_DIR)at91sam7s256-jtagkey-flash-program.cfg
OPENOCD_CFG = $(OPENOCD_DIR)at91sam7s256-armusbocd-flash-program.cfg

# program the AT91SAM7S256 internal flash memory
program: $(TARGET)
	@echo "Flash Programming with OpenOCD..."			# display a message on the console
	$(OPENOCD) -f $(OPENOCD_CFG)						# program the onchip FLASH here
	@echo "Flash Programming Finished."					# display a message on the console

